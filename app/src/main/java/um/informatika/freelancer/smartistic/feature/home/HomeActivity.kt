package um.informatika.freelancer.smartistic.feature.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_home.*
import um.informatika.freelancer.smartistic.R
import um.informatika.freelancer.smartistic.feature.about.AboutActivity
import um.informatika.freelancer.smartistic.feature.calculator.CalculatorActivity
import um.informatika.freelancer.smartistic.feature.materi.MateriActivity
import um.informatika.freelancer.smartistic.feature.quiz.ListQuizActivity

class HomeActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, HomeActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        toolbar.title = String.format("  %s", getString(R.string.app_name))
        toolbar.setLogo(R.drawable.logo_app_colored_48dp)
        setSupportActionBar(toolbar)

        text_materi.setOnClickListener { startActivity(MateriActivity.getIntent(this)) }
        text_quiz.setOnClickListener { startActivity(ListQuizActivity.getIntent(this)) }
        text_calculator.setOnClickListener { startActivity(CalculatorActivity.getIntent(this)) }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == R.id.menu_about) {
            startActivity(AboutActivity.getIntent(this))
            true
        } else super.onOptionsItemSelected(item)
    }
}
