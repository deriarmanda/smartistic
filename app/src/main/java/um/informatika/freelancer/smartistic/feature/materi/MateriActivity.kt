package um.informatika.freelancer.smartistic.feature.materi

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.item_list.view.*
import um.informatika.freelancer.smartistic.R
import um.informatika.freelancer.smartistic.feature.materi.detail.DetailMateriActivity
import um.informatika.freelancer.smartistic.util.Materi

class MateriActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, MateriActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        val list = listOf<Materi>(
            Materi.MATERI_BAB_1,
            Materi.MATERI_BAB_2,
            Materi.MATERI_BAB_3,
            Materi.MATERI_BAB_4
        )

        val rvAdapter = RvAdapter(list)
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@MateriActivity)
            adapter = rvAdapter
        }
    }

    inner class RvAdapter(val list: List<Materi>) : RecyclerView.Adapter<ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount() = list.size
        override fun onBindViewHolder(holder: ViewHolder, pos: Int) = holder.fetch(list[pos])
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(materi: Materi) {
            itemView.image_logo.setImageResource(R.drawable.ic_book_accent_32dp)
            itemView.text_title.text = materi.title
            itemView.text_subtitle.setText(materi.subTitleRes)
            itemView.container.setOnClickListener {
                startActivity(
                    DetailMateriActivity.getIntent(
                        this@MateriActivity,
                        materi
                    )
                )
            }
        }
    }
}
