package um.informatika.freelancer.smartistic.feature.materi.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_detail_materi.*
import um.informatika.freelancer.smartistic.R
import um.informatika.freelancer.smartistic.util.Materi

class DetailMateriActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_MATERI = "extra_materi"
        fun getIntent(context: Context, materi: Materi): Intent {
            val intent = Intent(context, DetailMateriActivity::class.java)
            intent.putExtra(EXTRA_MATERI, materi)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_materi)

        val materi = intent.getSerializableExtra(EXTRA_MATERI) as Materi
        supportActionBar?.title = materi.title
        supportActionBar?.setSubtitle(materi.subTitleRes)

        pdf_view.fromAsset(materi.pdfPath)
            .defaultPage(0)
            .enableSwipe(true)
            .swipeHorizontal(false)
            .spacing(0)
            .load()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }
}
