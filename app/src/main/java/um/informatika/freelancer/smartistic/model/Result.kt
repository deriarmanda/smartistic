package um.informatika.freelancer.smartistic.model

import android.os.Parcel
import android.os.Parcelable

data class Result(
    val title: String?,
    val score: Int,
    val rightAnswer: Int,
    val wrongAnswer: Int,
    val emptyAnswer: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeInt(score)
        parcel.writeInt(rightAnswer)
        parcel.writeInt(wrongAnswer)
        parcel.writeInt(emptyAnswer)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Result> {
        override fun createFromParcel(parcel: Parcel): Result {
            return Result(parcel)
        }

        override fun newArray(size: Int): Array<Result?> {
            return arrayOfNulls(size)
        }
    }
}