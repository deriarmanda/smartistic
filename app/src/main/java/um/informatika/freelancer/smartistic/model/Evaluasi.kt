package um.informatika.freelancer.smartistic.model

data class Evaluasi(
    val question: String,
    val answerKey: String,
    val answerA: String,
    val answerB: String,
    val answerC: String,
    val answerD: String,
    val answerE: String,
    var answer: String = "-"
)