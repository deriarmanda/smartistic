package um.informatika.freelancer.smartistic.feature.calculator

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_calculator.*
import um.informatika.freelancer.smartistic.R

class CalculatorActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, CalculatorActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)

        prepareForm()
        with(view_flipper) {
            isAutoStart = false
            setFlipInterval(500)
        }
        button_hitung.setOnClickListener { countAndShowResult() }
        button_kembali.setOnClickListener { goBackToFormPage() }
    }

    private fun prepareForm() {
        form_tempat_tidur.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_periode.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_perawatan.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_dirawat.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_pasien_hidup.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_pasien_mati_kurang.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_pasien_mati_lebih.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
    }

    private fun countAndShowResult() {
        val tempatTidur = Integer.parseInt(form_tempat_tidur.text.toString()).toFloat()
        val periode = Integer.parseInt(form_periode.text.toString()).toFloat()
        val perawatan = Integer.parseInt(form_perawatan.text.toString()).toFloat()
        val dirawat = Integer.parseInt(form_dirawat.text.toString()).toFloat()
        val pasienHidup = Integer.parseInt(form_pasien_hidup.text.toString()).toFloat()
        val pasienMatiKurang = Integer.parseInt(form_pasien_mati_kurang.text.toString()).toFloat()
        val pasienMatiLebih = Integer.parseInt(form_pasien_mati_lebih.text.toString()).toFloat()
        val pasienMati: Float = pasienMatiKurang + pasienMatiLebih

        // BOR
        text_bor_perawatan.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_jumlah_perawatan),
            perawatan
        )
        text_bor_tempat_tidur.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_tempat_tidur),
            tempatTidur
        )
        text_bor_periode.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_periode),
            periode
        )
        val hasilBor = (perawatan) / (tempatTidur * periode)
        text_hasil_bor.text = String.format(
            "%s = %.3f",
            getString(R.string.calc_text_hasil),
            hasilBor
        )

        // ALOS
        text_alos_dirawat.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_jumlah_lama_dirawat),
            dirawat
        )
        text_alos_pasien_hidup.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_hidup),
            pasienHidup
        )
        text_alos_pasien_mati.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_mati),
            pasienMati
        )
        val hasilAlos = (dirawat) / (pasienHidup + pasienMati)
        text_hasil_alos.text = String.format(
            "%s = %.3f",
            getString(R.string.calc_text_hasil),
            hasilAlos
        )

        // BTO
        text_bto_pasien_hidup.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_hidup),
            pasienHidup
        )
        text_bto_pasien_mati.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_mati),
            pasienMati
        )
        text_bto_tempat_tidur.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_tempat_tidur),
            tempatTidur
        )
        val hasilBto = (pasienHidup + pasienMati) / (tempatTidur)
        text_hasil_bto.text = String.format(
            "%s = %.3f",
            getString(R.string.calc_text_hasil),
            hasilBto
        )

        // TOI
        text_toi_tempat_tidur.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_tempat_tidur),
            tempatTidur
        )
        text_toi_periode.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_periode),
            periode
        )
        text_toi_perawatan.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_jumlah_perawatan),
            perawatan
        )
        text_toi_pasien_hidup.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_hidup),
            pasienHidup
        )
        text_toi_pasien_mati.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_mati),
            pasienMati
        )
        val hasilToi = ((tempatTidur * periode) - perawatan) / (pasienHidup + pasienMati)
        text_hasil_toi.text = String.format(
            "%s = %.3f",
            getString(R.string.calc_text_hasil),
            hasilToi
        )

        // NDR
        text_ndr_pasien_mati_lebih.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_mati_lebih_48_jam),
            pasienMatiLebih
        )
        text_ndr_pasien_hidup.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_hidup),
            pasienHidup
        )
        text_ndr_pasien_mati.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_mati),
            pasienMati
        )
        val hasilNdr = (pasienMatiLebih) / (pasienHidup + pasienMati)
        text_hasil_ndr.text = String.format(
            "%s = %.3f",
            getString(R.string.calc_text_hasil),
            hasilNdr
        )

        // GDR
        text_gdr_pasien_mati_kurang.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_mati),
            pasienMati
        )
        text_gdr_pasien_hidup.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_hidup),
            pasienHidup
        )
        text_gdr_pasien_mati.text = String.format(
            "%s = %.1f",
            getString(R.string.calc_hint_keluar_mati),
            pasienMati
        )
        val hasilGdr = (pasienMati) / (pasienHidup + pasienMati)
        text_hasil_gdr.text = String.format(
            "%s = %.3f",
            getString(R.string.calc_text_hasil),
            hasilGdr
        )

        with(view_flipper) {
            setInAnimation(context, R.anim.in_from_right)
            setOutAnimation(context, R.anim.out_to_left)
            displayedChild = 1
        }
    }

    private fun goBackToFormPage() {
        with(view_flipper) {
            setInAnimation(context, R.anim.in_from_left)
            setOutAnimation(context, R.anim.out_to_right)
            displayedChild = 0
        }
    }
}
