package um.informatika.freelancer.smartistic.feature.quiz.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_detail_quiz.*
import kotlinx.android.synthetic.main.item_quiz.view.*
import um.informatika.freelancer.smartistic.R
import um.informatika.freelancer.smartistic.feature.quiz.result.ResultActivity
import um.informatika.freelancer.smartistic.model.Evaluasi
import um.informatika.freelancer.smartistic.model.Result
import um.informatika.freelancer.smartistic.util.ListEvaluasi

class DetailQuizActivity : AppCompatActivity() {

    private val list = arrayListOf<Evaluasi>()

    companion object {
        private const val EXTRA_EVALUASI = "extra_evaluasi"
        fun getIntent(context: Context, listEvaluasi: ListEvaluasi): Intent {
            val intent = Intent(context, DetailQuizActivity::class.java)
            intent.putExtra(EXTRA_EVALUASI, listEvaluasi)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_quiz)

        val evaluasi = intent.getSerializableExtra(EXTRA_EVALUASI) as ListEvaluasi
        supportActionBar?.title = evaluasi.title
        supportActionBar?.setSubtitle(evaluasi.subTitleRes)

        prepareQuiz(evaluasi)

        with(list_quiz) {
            layoutManager = LinearLayoutManager(this@DetailQuizActivity)
            adapter = RvAdapter()
        }

        button_selesai.setOnClickListener { countScore(evaluasi.title) }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun prepareQuiz(listEvaluasi: ListEvaluasi) {
        list.clear()
        val questions = resources.getStringArray(listEvaluasi.questionRes)
        val keys = resources.getStringArray(listEvaluasi.answerKeyRes)
        val answerAs = resources.getStringArray(listEvaluasi.answerARes)
        val answerBs = resources.getStringArray(listEvaluasi.answerBRes)
        val answerCs = resources.getStringArray(listEvaluasi.answerCRes)
        val answerDs = resources.getStringArray(listEvaluasi.answerDRes)
        val answerEs = resources.getStringArray(listEvaluasi.answerERes)

        for (i in 0 until questions.size) {
            list.add(
                Evaluasi(
                    questions[i],
                    keys[i],
                    answerAs[i],
                    answerBs[i],
                    answerCs[i],
                    answerDs[i],
                    answerEs[i]
                )
            )
        }
    }

    private fun countScore(title: String) {
        var rightAnswer = 0
        var wrongAnswer = 0
        var emptyAnswer = 0

        for (evaluasi in list) {
            when {
                evaluasi.answer == evaluasi.answerKey -> rightAnswer++
                evaluasi.answer == "-" -> emptyAnswer++
                else -> wrongAnswer++
            }
        }

        val score = (rightAnswer * 100) / list.size
        val result = Result(
            title, score, rightAnswer, wrongAnswer, emptyAnswer
        )

        startActivity(ResultActivity.getIntent(this, result))
        finish()
    }

    inner class RvAdapter : RecyclerView.Adapter<ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_quiz, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount() = list.size
        override fun onBindViewHolder(holder: ViewHolder, pos: Int) = holder.fetch(list[pos], pos + 1)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(evaluasi: Evaluasi, number: Int) {
            itemView.text_number.text = String.format("%d.", number)
            itemView.text_question.text = evaluasi.question
            itemView.answer_a.text = evaluasi.answerA
            itemView.answer_b.text = evaluasi.answerB
            itemView.answer_c.text = evaluasi.answerC
            itemView.answer_d.text = evaluasi.answerD
            itemView.answer_e.text = evaluasi.answerE

            itemView.group_answer.apply {
                check(
                    when (evaluasi.answer) {
                        "A" -> R.id.answer_a
                        "B" -> R.id.answer_b
                        "C" -> R.id.answer_c
                        "D" -> R.id.answer_d
                        "E" -> R.id.answer_e
                        else -> -1
                    }
                )

                setOnCheckedChangeListener { _, id ->
                    when (id) {
                        R.id.answer_a -> evaluasi.answer = "A"
                        R.id.answer_b -> evaluasi.answer = "B"
                        R.id.answer_c -> evaluasi.answer = "C"
                        R.id.answer_d -> evaluasi.answer = "D"
                        R.id.answer_e -> evaluasi.answer = "E"
                    }
                }
            }
        }
    }
}
