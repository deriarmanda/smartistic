package um.informatika.freelancer.smartistic.util

import um.informatika.freelancer.smartistic.R

enum class ListEvaluasi(
    val title: String,
    val subTitleRes: Int,
    val questionRes: Int,
    val answerKeyRes: Int,
    val answerARes: Int,
    val answerBRes: Int,
    val answerCRes: Int,
    val answerDRes: Int,
    val answerERes: Int
) {
    BAB_1(
        "Evaluasi Bab 1",
        R.string.title_bab_1,
        R.array.quiz_question_bab_1,
        R.array.quiz_answer_key_bab_1,
        R.array.quiz_answer_a_bab_1,
        R.array.quiz_answer_b_bab_1,
        R.array.quiz_answer_c_bab_1,
        R.array.quiz_answer_d_bab_1,
        R.array.quiz_answer_e_bab_1
    ),
    BAB_2(
        "Evaluasi Bab 2",
        R.string.title_bab_2,
        R.array.quiz_question_bab_2,
        R.array.quiz_answer_key_bab_2,
        R.array.quiz_answer_a_bab_2,
        R.array.quiz_answer_b_bab_2,
        R.array.quiz_answer_c_bab_2,
        R.array.quiz_answer_d_bab_2,
        R.array.quiz_answer_e_bab_2
    ),
    BAB_3(
        "Evaluasi Bab 3",
        R.string.title_bab_3,
        R.array.quiz_question_bab_3,
        R.array.quiz_answer_key_bab_3,
        R.array.quiz_answer_a_bab_3,
        R.array.quiz_answer_b_bab_3,
        R.array.quiz_answer_c_bab_3,
        R.array.quiz_answer_d_bab_3,
        R.array.quiz_answer_e_bab_3
    ),
    BAB_4(
        "Evaluasi Bab 4",
        R.string.title_bab_4,
        R.array.quiz_question_bab_4,
        R.array.quiz_answer_key_bab_4,
        R.array.quiz_answer_a_bab_4,
        R.array.quiz_answer_b_bab_4,
        R.array.quiz_answer_c_bab_4,
        R.array.quiz_answer_d_bab_4,
        R.array.quiz_answer_e_bab_4
    ),
    SEMUA_BAB(
        "Evaluasi Akhir",
        R.string.title_semua_bab,
        R.array.quiz_question_evaluasi,
        R.array.quiz_answer_key_evaluasi,
        R.array.quiz_answer_a_evaluasi,
        R.array.quiz_answer_b_evaluasi,
        R.array.quiz_answer_c_evaluasi,
        R.array.quiz_answer_d_evaluasi,
        R.array.quiz_answer_e_evaluasi
    ),
}