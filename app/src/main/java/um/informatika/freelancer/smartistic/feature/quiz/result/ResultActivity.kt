package um.informatika.freelancer.smartistic.feature.quiz.result

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_result.*
import nl.dionsegijn.konfetti.models.Shape
import nl.dionsegijn.konfetti.models.Size
import um.informatika.freelancer.smartistic.R
import um.informatika.freelancer.smartistic.model.Result

class ResultActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_RESULT = "extra_result"
        fun getIntent(context: Context, result: Result): Intent {
            val intent = Intent(context, ResultActivity::class.java)
            intent.putExtra(EXTRA_RESULT, result)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val result = intent.getParcelableExtra<Result>(EXTRA_RESULT)

        text_title.text = getString(R.string.result_msg_title, result.title)
        text_score.text = result.score.toString()
        text_right_answer.text = getString(R.string.result_text_jawaban_benar, result.rightAnswer)
        text_wrong_answer.text = getString(R.string.result_text_jawaban_salah, result.wrongAnswer)
        text_empty_answer.text = getString(R.string.result_text_jawaban_kosong, result.emptyAnswer)

        konfetti.build()
            .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
            .setDirection(0.0, 359.0)
            .setSpeed(5f)
            .setFadeOutEnabled(true)
            .setTimeToLive(60000L)
            .addShapes(Shape.RECT, Shape.CIRCLE)
            .addSizes(Size(12))
            .setPosition(-50f, konfetti.width + 250f, -50f, -50f)
            .streamFor(300, 300000L)
    }
}
