package um.informatika.freelancer.smartistic.feature.about

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import um.informatika.freelancer.smartistic.R

class AboutActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, AboutActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
    }
}
