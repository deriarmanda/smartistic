package um.informatika.freelancer.smartistic.feature.quiz

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.item_list.view.*
import um.informatika.freelancer.smartistic.R
import um.informatika.freelancer.smartistic.feature.quiz.detail.DetailQuizActivity
import um.informatika.freelancer.smartistic.util.ListEvaluasi

class ListQuizActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, ListQuizActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        val list = listOf(
            ListEvaluasi.BAB_1, ListEvaluasi.BAB_2, ListEvaluasi.BAB_3,
            ListEvaluasi.BAB_4, ListEvaluasi.SEMUA_BAB
        )

        val rvAdapter = RvAdapter(list)
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@ListQuizActivity)
            adapter = rvAdapter
        }
    }

    inner class RvAdapter(private val list: List<ListEvaluasi>) : RecyclerView.Adapter<ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount() = list.size
        override fun onBindViewHolder(holder: ViewHolder, pos: Int) = holder.fetch(list[pos])
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(listEvaluasi: ListEvaluasi) {
            itemView.image_logo.setImageResource(R.drawable.ic_test_accent_32dp)
            itemView.text_title.text = listEvaluasi.title
            itemView.text_subtitle.setText(listEvaluasi.subTitleRes)
            itemView.container.setOnClickListener {
                startActivity(
                    DetailQuizActivity.getIntent(
                        this@ListQuizActivity,
                        listEvaluasi
                    )
                )
            }
        }
    }
}
