package um.informatika.freelancer.smartistic.util

import um.informatika.freelancer.smartistic.R

enum class Materi(
    val title: String,
    val subTitleRes: Int,
    val pdfPath: String
    //,val listEvaluasi: ListEvaluasi = ListEvaluasi.SEMUA_BAB
) {
    MATERI_BAB_1(
        "Materi Bab 1",
        R.string.title_bab_1,
        "materi_pengantar.pdf"
    ),
    MATERI_BAB_2(
        "Materi Bab 2",
        R.string.title_bab_2,
        "materi_sensus.pdf"
    ),
    MATERI_BAB_3(
        "Materi Bab 3",
        R.string.title_bab_3,
        "materi_indikator.pdf"
    ),
    MATERI_BAB_4(
        "Materi Bab 4",
        R.string.title_bab_4,
        "materi_grafik.pdf"
    ),
}